#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk


class Archivo():
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/mainwindow.ui")

        self.SearchFile = self.builder.get_object("importfile")
        self.SearchFile.set_title("Seleccione un archivo PDB")
        self.SearchFile.resize(600, 400)
        self.SearchFile.show_all()

        # boton cancelar
        Cancelar = self.SearchFile.add_button(Gtk.STOCK_CANCEL,
                                              Gtk.ResponseType.CANCEL)
        Cancelar.set_always_show_image(True)
        Cancelar.connect("clicked", self.Cerrar)

        # boton aceptar
        Aceptar = self.SearchFile.add_button(Gtk.STOCK_OK,
                                             Gtk.ResponseType.OK)
        Aceptar.set_always_show_image(True)
        Aceptar.connect("clicked", self.ArchivoSearch)

    def Cerrar(self, btn=None):
        self.SearchFile.hide()

    # Que archivo se fue seleccionado
    def ArchivoSearch(self, btn=None):
        self.archive = self.SearchFile.get_filename()
        if self.archive is not None:
            self.SearchFile.hide()
            pass
        pass
