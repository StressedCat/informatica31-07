#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import __main__
from biopandas.pdb import PandasPdb
# Pymol: quiet  and no GUI
__main__.pymol_argv = ['pymol', '-qc']
import pymol

pymol.finish_launching()


class Visualizar():

    def __init__(self, path):
        """
        se hace el proceso de crear una imagen visual del pdb seleccionado,
        junto a esto el archivo png tendrá el nombre del pdb ingresado
        """
        pdb_file = path
        ppdb = PandasPdb()
        ppdb.read_pdb(path)
        pdb_name = ppdb.code
        pymol.cmd.load(pdb_file, pdb_name)
        pymol.cmd.disable("all")
        pymol.cmd.enable(pdb_name)
        pymol.cmd.hide('all')
        pymol.cmd.show('cartoon')
        pymol.cmd.set('ray_opaque_background', 0)
        pymol.cmd.pretty(pdb_name)
        pymol.cmd.png("%s.png" % (pdb_name))
        self.PNG = ("%s.png" % (pdb_name))


class Biopand(object):

    def __init__(self, path):
        """
        aca se lee el archivo pdb para obtener la informacion de este, el cual
        estará guardado con la forma de un ATOM.pdb, ademas se consiguen cosas
        basicas como el nombre
        """
        ppdb = PandasPdb()
        ppdb.read_pdb(path)
        self.PdbName = ppdb.code
        self.PdbContent = ('\nRaw PDB file contents:\n\n %s\n...'
                           % ppdb.pdb_text[:1000])
        self.keys = (ppdb.df.keys())
        # print(ppdb.df.keys())
        # print('\nRaw PDB file contents:\n\n %s\n...' % ppdb.pdb_text[:1000])
        # print(ppdb.df["ATOM"].head())

        ppdb.to_pdb(path='ATOM.pdb',
                    records=['ATOM'],
                    gz=False,
                    append_newline=True)
