#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk


class dlgInfo():
    def __init__(self, name, File):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/mainwindow.ui")

        self.InfoExtra = self.builder.get_object("dlgAvanzado")
        self.InfoExtra.set_title("Informacion de la molécula")
        self.InfoExtra.resize(600, 400)
        self.InfoExtra.show_all()

        # el nombre de la proteina
        self.labelinfo = self.builder.get_object("NameMol")
        self.labelinfo.set_label(name)

        # contenido en el textview
        self.content = self.builder.get_object("Buffer")
        self.content.set_text(File)

        # donde estara almacenado el texto
        self.text = self.builder.get_object("Text")

        Cancelar = self.InfoExtra.add_button(Gtk.STOCK_CANCEL,
                                             Gtk.ResponseType.CANCEL)
        Cancelar.set_always_show_image(True)
        Cancelar.connect("clicked", self.Cerrar)

        # boton aceptar
        Aceptar = self.InfoExtra.add_button(Gtk.STOCK_OK,
                                            Gtk.ResponseType.OK)
        Aceptar.set_always_show_image(True)
        # Aceptar.connect("clicked", self.ArchivoSearch)

    def Cerrar(self, btn=None):
        self.InfoExtra.hide()
