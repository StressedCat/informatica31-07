#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
import time
from importar import Archivo
from Visualizar import Visualizar
from Visualizar import Biopand
from dlgAvanzado import dlgInfo
from dlgAbout import AboutGTK

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk


class wnPrincipal():
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/mainwindow.ui")

        # Ventana principal
        self.window = self.builder.get_object("window")
        self.window.connect("destroy", Gtk.main_quit)
        self.window.set_title("Ventana Ejemplo")
        self.window.maximize()
        self.window.show_all()

        # Boton para visualizar el archivo pdb seleccionado
        self.btnPrint = self.builder.get_object("SeePDB")
        self.btnPrint.set_label("Visualizar PDB")
        self.btnPrint.connect("clicked", self.VisualizarPDB)

        # boton para recibir mas informacion del pdb
        self.btnInfo = self.builder.get_object("AdvInfo")
        self.btnInfo.set_label("Informacion avanzada")
        self.btnInfo.connect("clicked", self.InfoMol)

        # Boton para conseguir un archivo pdb
        File = self.builder.get_object("btnAdd")
        File.set_label("Añadir archivo")
        File.connect("clicked", self.ImportarDef)

        self.menu = self.builder.get_object("Menu")

        self.btnAbout = self.builder.get_object("About")
        self.btnAbout.set_label("acerca de ...")
        self.btnAbout.connect("clicked", self.about)


        # se añadira el nombre de la molécula, usualmente en forma de codigo
        self.label = self.builder.get_object("Info")
        self.label.set_text("Molécula: ")

        # Es la lista donde se almacenan los archivos pdb
        self.lista = self.builder.get_object("List")
        self.lista.connect("cursor-changed", self.lista_select)
        self.model = Gtk.ListStore(str)
        self.lista.set_model(model=self.model)

        # La imagen del pdb
        self.Imagen = self.builder.get_object("Imagen")

        cell = Gtk.CellRendererText()

        column = Gtk.TreeViewColumn(title="Lista",
                                    cell_renderer=cell,
                                    text=0)

        self.lista.append_column(column)
        self.model.append(["prueba"])

    # Que se selecciono
    def lista_select(self, tree=None):
        model, it = self.lista.get_selection().get_selected()
        if model is None:
            return

    # Informacion del programa
    def about(self, btn=None):
        about = AboutGTK()
        about.Aboutwindow.run()
        pass

    # Importar algun archivo pdb
    def ImportarDef(self, btn=None):
        dlg = Archivo()
        response = dlg.SearchFile.run()
        if response == Gtk.ResponseType.OK:
            self.model.append([dlg.archive])
            pass
        pass

    # Visualizar el pdb
    def VisualizarPDB(self, btn=None):
        # Se ve que se selecciono
        model, it = self.lista.get_selection().get_selected()
        if model is None or it is None:
            return
        else:
            # Se consigue la imagen de la molecula
            png = Visualizar(path=(model.get_value(it, 0)))
            # Se consigue la informacion del pdb, para que tenga un nombre
            infopdb = Biopand(path=(model.get_value(it, 0)))
            name = "Molécula: " + infopdb.PdbName
            self.label.set_text(name)
            # Se hace delay ya que visualizar no es instantaneo
            time.sleep(3)
            self.Imagen.set_from_file(png.PNG)
        pass

    # Informacion del pdb
    def InfoMol(self, btn=None):
        # Se ve que se tiene seleccionado
        model, it = self.lista.get_selection().get_selected()
        if model is None or it is None:
            return
        # Se consigue la informacion del pdb
        infopdb = Biopand(path=(model.get_value(it, 0)))
        name = "Molécula: " + infopdb.PdbName
        filePdb = infopdb.PdbContent
        info = dlgInfo(name=name,
                       File=filePdb)
        info.InfoExtra.run()
        pass


if __name__ == "__main__":
    inicio = wnPrincipal()
    Gtk.main()
