#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk


class AboutGTK():

    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/mainwindow.ui")

        self.Aboutwindow = self.builder.get_object("dlgAbout")
        self.Aboutwindow.set_title("Acerca del programa")
        self.Aboutwindow.resize(400, 250)
        self.Aboutwindow.show_all()
